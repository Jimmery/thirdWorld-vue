const getters = {
    headTitle: state => state.headTitle,
    themeColor: state => state.themeColor
}
export default getters
